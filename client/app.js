import './app.css'

import Vue from 'vue'

import App from './components/App.vue'
import router from './router/index.js'
import store from './store/index.js'
import {sync} from 'vuex-router-sync'

// Vue plugins
import wsPlugin from './services/ws.js'
import servicesPlugin from './services/services.js'
import modulesPlugin from './services/modules.js'
import i18nPlugin from './services/i18n.js'
import utilsPlugin from './services/utils.js'
import offlinedbPlugin from './services/offlinedb.js'

// in some way is a plugin
import * as d3 from 'd3'

// Vuetify
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

// File upload
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'

// recursive components
import JSONFormItem from './components/iio/JSONFormItem.vue'
import SchemaTreeItem from './components/iio/SchemaTreeItem.vue'
import JSONViewer from './components/iio/JSONViewer.vue'
// other components
import JSONForm from './components/iio/JSONForm.vue'
import FileDrop from './components/iio/FileDrop.vue'
import ColorPicker from './components/iio/ColorPicker.vue'
import Geo from './components/iio/Geo.vue'

// additional inititalizations
import additionalInit from './app-additional-init.js'

// additional post inititalizations
import additionalPostInit from './app-additional-postinit.js'

// router sync
sync(store, router)

// Vue plugins use
Vue.use(Vuetify)
Vue.use(wsPlugin)
Vue.use(servicesPlugin)
Vue.use(utilsPlugin)
Vue.use(modulesPlugin)
Vue.use(i18nPlugin)
Vue.use(offlinedbPlugin)

// additional inititalizations
additionalInit()

// make Vue available for services
global.Vue = Vue

// inject d3 as used very often
Vue.prototype.$d3 = d3

// create app
const app = new Vue({
  router,
  store,
  ...App
})

// initialize modules
app.$modules.init()

// File upload
Vue.component('file-uploader', vue2Dropzone)

// recursive components
Vue.component('jsonform-item', JSONFormItem)
Vue.component('schematree-item', SchemaTreeItem)
Vue.component('jsonviewer', JSONViewer)
// other components
Vue.component('jsonform', JSONForm)
Vue.component('file-drop', FileDrop)
Vue.component('colorpicker', ColorPicker)
Vue.component('geo', Geo)

// on service up (unified services)
app.$services.$on('service:up', service => {
  if (!service) {
    console.log('something weird there when service up')
    return
  }

  switch (service.name) {
    case 'auth':
      if (app.$store.state.user) {
        app.$services.auth.authenticate({
          username: app.$store.state.user.username,
          token: localStorage.token
        }).then(() => {
          console.log('authenticated')
          app.$ws.socket._logged = true
          app.$services.$emit('app:login')
        }).catch(err => {
          console.log('authentication failed', err)
          // app.$ws.resetLocalCredentials()
          // app.$router.push('/login')
          app.$services.$emit('app:logout')
        })
      }
      break
    default:
  }
})

// additional post inititalizations
additionalPostInit(app)

// manage splashscreen/progress
app.$d3.select('#splashscreen').style('opacity', 0)
app.$d3.select('#progress').style('opacity', 0)

setTimeout(() => {
  app.$d3.select('#splashscreen').remove()
  app.$d3.select('#progress').remove()
}, 1000)

export {app, router, store}
