import _ from 'lodash'
import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'

// import additional state info for a given app
import appState from './app-state'
//

Vue.use(Vuex)

const state = _.merge({
  translation: '',
  user: null,
  otherUser: null,
  geoCenter: null,
  config: null,
  rest: null,
  toolbarColor: 'transparent',
  flatToolbar: true,
  darkTheme: true,
  section: null,
  showSection: true,
  connected: true
}, appState.state)

const defaultState = _.cloneDeep(state)

const mutations = _.merge({
  translation(state, value) {
    state.translation = value
  },
  user(state, value) {
    state.user = value
  },
  otherUser(state, value) {
    state.otherUser = value
  },
  geoCenter(state, value) {
    state.geoCenter = value
  },
  rest(state, value) {
    state.rest = value
  },
  config(state, value) {
    state.config = value
  },
  toolbarColor(state, value) {
    state.toolbarColor = value
  },
  flatToolbar(state, value) {
    state.flatToolbar = value
  },
  darkTheme(state, value) {
    state.darkTheme = value
  },
  section(state, value) {
    state.section = value
  },
  showSection(state, value) {
    state.showSection = value
  },
  connected(state, value) {
    state.connected = value
  }
}, appState.mutations)

const actions = _.merge({

}, appState.actions)

const store = new Vuex.Store({
  state,
  mutations,
  actions,
  plugins: [ createPersistedState() ]
})

export default store
