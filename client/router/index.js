import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/Login.vue'
import Dashboard from '../views/Dashboard.vue'
import Profile from '../views/Profile.vue'
import Users from '../views/Users.vue'
import EditUser from '../views/EditUser.vue'
import Admin from '../views/Admin.vue'

import Services from '../views/Services.vue'

// import additional routes defined for a given app
import appRoutes from './app-routes'
//

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      component: Dashboard,
      beforeEnter: (to, from, next) => {
        let token = localStorage.getItem('token')
        if (token && token !== 'null') {
          next()
        } else {
          next({ path: '/login' })
        }
      }
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/profile',
      component: Profile,
      beforeEnter: (to, from, next) => {
        let token = localStorage.getItem('token')
        if (token && token !== 'null') {
          next()
        } else {
          next({ path: '/login' })
        }
      }
    },
    {
      path: '/users',
      component: Users,
      beforeEnter: (to, from, next) => {
        let token = localStorage.getItem('token')
        if (token && token !== 'null') {
          next()
        } else {
          next({ path: '/login' })
        }
      }
    },
    {
      path: '/edituser',
      component: EditUser,
      beforeEnter: (to, from, next) => {
        let token = localStorage.getItem('token')
        if (token && token !== 'null') {
          next()
        } else {
          next({ path: '/login' })
        }
      }
    },
    {
      path: '/admin',
      component: Admin,
      beforeEnter: (to, from, next) => {
        let token = localStorage.getItem('token')
        if (token && token !== 'null') {
          next()
        } else {
          next({ path: '/login' })
        }
      }
    },
    {
      path: '/services',
      component: Services
    },
    {
      path: '*',
      redirect: '/'
    }
  ].concat(appRoutes)
})
