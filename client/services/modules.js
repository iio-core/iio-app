import Vue from 'vue'
import store from '../store'

class ModulesService extends Vue {
  constructor() {
    super({ store })

    this.socket = this.$ws.socket
    this.uuid = Math.random().toString(36).slice(2)

    // listeners
    this._onUpdateModules = this._updateModules.bind(this)

    // register special app methods
    this.register('app', 'modules')
    this.register('app', 'generateDataFormJSONSchema')
    this.register('app', 'info')
    this.register('app', 'restAPIKeys')
  }

  init() {
    this.waitForService('app').then(app => {
      this._updateModules()

      this.waitForProperty(this, '$services').then($services => {
        $services.$on('service:up', this._onUpdateModules)
      }).catch(err => console.log(err))
    })
  }

  _updateModules() {
    this.app.modules().then(result => {
      // console.log('modules', result)
      for (let s in result.list) {
        let service = result.list[s]

        // root level methods
        if (service.methods) {
          for (let method in service.methods) {
            this.register(service.name, method)
          }
        }

        // sub services and their methods
        if (service.subs) {
          for (let subservice in service.subs) {
            for (let method of service.subs[subservice]) {
              this.register(service.name + ':' + subservice, method)
            }
          }
        }
      }
    }).catch(err => {
      console.log(err)
    })
  }

  register(service, method) {
    let setMethod = serviceName => {
      return args => {
        return new Promise((resolve, reject) => {
          let token = Math.random().toString(36).slice(2)
          let topic = 'module:' + serviceName + ':' + method + ':' + token

          let timeout = setTimeout(() => {
            this.socket.off(topic)
            reject(new Error('timeout for ' + topic))
          }, 30000)

          this.socket.once(topic, data => {
            clearTimeout(timeout)
            if (data.err) {
              reject(data.err)
            } else {
              resolve(data.result)
            }

            // heartbeat
            this.$ws.heartbeat = true
          })

          // 2018/08/15: tokenized userID
          let fullArgs = {
            topic: 'module:' + serviceName + ':request',
            args: args,
            token: token,
            method: method,
            userId: localStorage.token
          }

          this.socket.emit('module:event', fullArgs)
        })
      }
    }

    // if subservices concerned
    if (service.match(':')) {
      let subservice = service.split(':')[1]
      service = service.split(':')[0]

      this[service] = this[service] || {}
      this[service][subservice] = this[service][subservice] || {}

      this[service][subservice][method] = setMethod(service + ':' + subservice)
    // ... else only root level methods
    } else {
      this[service] = this[service] || {}

      this[service][method] = setMethod(service)
    }
  }

  waitForService(name, sub) {
    return new Promise((resolve, reject) => {
      var checkTimeout

      var checkInterval = setInterval(() => {
        if (sub) {
          if (this[name] && this[name][sub]) {
            clearInterval(checkInterval)
            clearTimeout(checkTimeout) // nothing if undefined

            resolve(this[name][sub])
          }
        } else {
          if (this[name]) {
            clearInterval(checkInterval)
            clearTimeout(checkTimeout) // nothing if undefined

            resolve(this[name])
          }
        }
      }, 100)

      checkTimeout = setTimeout(() => {
        if (checkInterval) {
          clearInterval(checkInterval)
          if (sub) {
            reject(new Error('Timeout: subservice ' + sub + ' of service ' + name + ' is not available'))
          } else {
            reject(new Error('Timeout: service ' + name + ' is not available'))
          }
        }
      }, 5000)
    })
  }

  waitForProperty(obj, property, delay) {
    return new Promise((resolve, reject) => {
      var checkTimeout

      var checkInterval = setInterval(() => {
        if (obj[property]) {
          clearInterval(checkInterval)
          clearTimeout(checkTimeout) // nothing if undefined

          resolve(obj[property])
        }
      }, 100)

      checkTimeout = setTimeout(() => {
        if (checkInterval) {
          clearInterval(checkInterval)
          reject(new Error('Timeout: property ' + property + ' is not available'))
        }
      }, delay || 5000)
    })
  }
}

var modulesPlugin = {
  install:
    function (Vue) {
      Vue.prototype.$modules = new ModulesService()
    }
}

export default modulesPlugin
