import Vue from 'vue'
import store from '../store'

import dayjs from 'dayjs'
import relativeTime from 'dayjs/plugin/relativeTime'
dayjs.extend(relativeTime)
import '../i18n/fr'

import * as d3 from 'd3'
import _ from 'lodash'
import loadjs from 'loadjs'

class UtilsService extends Vue {
  constructor() {
    super({ store })

    this.uuid = Math.random().toString(36).slice(2)

    this._users = {}

    this.$services.waitForService('config').then(config => {
      config.get().then(data => {
        this.bucket = data.store.bucket
      }).catch(err => console.log(err))
    }).catch(err => console.log(err))
  }

  async getConfig() {
    let config
    if (this.$store.state.config && !this.$store.state.connected) {
      config = this.$store.state.config
    } else {
      try {
        await this.$services.waitForService('config')
        config = await this.$services.config.get()
      } catch (err) {
        console.log(err)
      }
    }

    return config
  }

  /* used for deep update in JS object given a path description */
  updatePropertyByPath(obj, path, val, removal) {
    // console.log(JSON.stringify(obj, null, 2), path, val)
    if (path.match(/\./)) {
      // console.log('path=', path, ', val=', JSON.stringify(val))
      let pathArray = path.split('.')
      let propPath = pathArray.slice(1).join('.')
      let prop = pathArray[0]
      if (prop.match(/\[.*\]/)) {
        let arrName = prop.slice(0, prop.indexOf('['))
        let idx = parseInt(path.match(/\[.*\]/)[0].replace('[', '').replace(']', ''))

        obj[arrName] = obj[arrName] || []
        obj[arrName][idx] = obj[arrName][idx] || {}
        this.updatePropertyByPath(obj[arrName][idx], propPath, val, removal)
      } else {
        this.updatePropertyByPath(obj[prop], propPath, val, removal)
      }
    } else if (path.match(/\[/)) {
      let count = path.match(/\[/g).length
      if (count === 1) {
        let idx = parseInt(path.match(/\[.*\]/)[0].replace('[', '').replace(']', ''))
        let prop = path.match(/.*\[/)[0].replace('[', '')

        if (!removal) {
          obj[prop][idx] = val
          // console.log('idx', idx, 'prop', prop, obj[prop][idx])
        } else {
          obj[prop].splice(idx, 1)
          // console.log('idx removal ', idx, 'prop', prop)
        }
      } else {
        let matches = path.match(/\[\d\]/g)
        let idxs = _.map(matches, function(e) {
          return parseInt(e.replace('[', '').replace(']', ''))
        })

        let prop = path.match(/\w+\[/)[0].replace('[', '')

        if (!removal) {
          // manage array of arrays: to be improved
          // console.log('prop = ', obj[prop], 'val = ', val, JSON.stringify(obj[prop]))

          obj[prop] = obj[prop] || []
          // console.log(JSON.stringify(obj[prop]))
          let fullObj = obj[prop]
          for (let i = 0; i < idxs.length; i++) {
            if (i < idxs.length - 1) {
              fullObj[idxs[i]] = fullObj[idxs[i]] || []
              fullObj = fullObj[idxs[i]]
            } else {
              fullObj[idxs[i]] = parseFloat(val)
            }
          }

          // console.log('idxs', idxs, 'prop', prop, fullObj, JSON.stringify(obj[prop]))
        } else {
          obj[prop].splice(idxs[0], 1)
          // console.log('idx removal ', idx, 'prop', prop)
        }
      }
    } else {
      obj[path] = val
    }
  }

  fromNow(date) {
    let lang = this.$i18n._language.split('-')[0]
    return date ? dayjs(date).locale(lang).fromNow() : ''
  }

  prettyDate(date) {
    let lang = this.$i18n._language.split('-')[0]
    return date ? dayjs(date).locale(lang).format('DD MMMM YYYY') : ''
  }

  prettyDateAndTime(date) {
    let lang = this.$i18n._language.split('-')[0]
    return date ? dayjs(date).locale(lang).format('DD MMMM YYYY  hh:mm') : ''
  }

  loadFrappe(version = '1.1.0') {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('frappe')) {
        loadjs(
          'https://unpkg.com/frappe-charts@ ' +
          version + '/dist/frappe-charts.min.iife.js',
          'frappe', {
            success: () => {
              // console.log('frappe', global.frappe)
              resolve(global.frappe)
            },
            error: err => reject(err)
          })
      } else {
        resolve(global.frappe)
      }
    })
  }

  loadEcharts(version = '4.0.4') {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('echarts')) {
        loadjs(
          'https://cdnjs.cloudflare.com/ajax/libs/echarts/' + version + '/echarts.min.js',
          'echarts', {
            success: () => {
              // console.log('echarts', global.echarts)
              resolve(global.echarts) },
            error: err => reject(err)
          })
      } else {
        resolve(global.echarts)
      }
    })
  }

  loadChartJSRTPlugin() {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('ChartJS-RT')) {
        loadjs('/js/chartjs-plugin-streaming.js',
          'ChartJS-RT', {
            success: () => {
              // console.log('Chart plugin', global.Chart)
              resolve(global.Chart)
            },
            error: err => reject(err)
          })
      } else {
        resolve(global.Chart)
      }
    })
  }

  loadApexCharts() {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('ApexCharts')) {
        loadjs('https://cdn.jsdelivr.net/npm/apexcharts',
          'ApexCharts', {
            success: () => {
              // console.log('ApexCharts', global.ApexCharts)
              resolve(global.ApexCharts)
            },
            error: err => reject(err)
          })
      } else {
        resolve(global.ApexCharts)
      }
    })
  }

  loadChartJS(version = '2.7.2') {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('ChartJS')) {
        loadjs(
          [ 
            'https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js',
            'https://cdnjs.cloudflare.com/ajax/libs/Chart.js/' + version + '/Chart.bundle.min.js'
          ],
          'ChartJS', {
            success: () => {
              // console.log('Chart', global.Chart)
              resolve(global.Chart)
            },
            error: err => reject(err)
          })
      } else {
        resolve(global.Chart)
      }
    })
  }

  loadOpenLayers(version = '4.6.5') {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('openlayers')) {
        loadjs(
          [
            'https://cdnjs.cloudflare.com/ajax/libs/openlayers/' + version + '/ol.css',
            'https://cdnjs.cloudflare.com/ajax/libs/openlayers/' + version + '/ol.js'
          ], 'openlayers', {
            success: () => {
              // console.log('openlayers', global.ol)
              resolve(global.ol)
            },
            error: err => reject(err)
          })
      } else {
        resolve(global.ol)
      }
    })
  }

  loadDropzone(version = '5.5.1') {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('dropzone')) {
        loadjs(
          [
            'https://cdnjs.cloudflare.com/ajax/libs/dropzone/' + version + '/min/basic.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/dropzone/' + version + '/min/dropzone.min.css',
            'https://cdnjs.cloudflare.com/ajax/libs/dropzone/' + version + '/min/dropzone.min.js'
          ], 'dropzone', {
            success: () => {
              // console.log('dropzone', global.Dropzone)
              resolve(global.Dropzone)
            },
            error: err => reject(err)
          })
      } else {
        resolve(global.ol)
      }
    })
  }

  loadAsync(what, whichKind) {
    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest()

      req.addEventListener('error', function(err) {
        reject(err)
      })

      // report progress events
      req.addEventListener('progress', function(event) {

      }, false)

      // load responseText into a new script element
      req.addEventListener('load', function(event) {
        let e = event.target
        let uid = 'la_' + Math.random().toString(36).slice(2, 12)

        if (e.responseText) {
          var containerElement

          switch (whichKind) {
            case 'js':
              containerElement = document.createElement('script')
              break
            case 'css':
              containerElement = document.createElement('style')
              break
            default:
              containerElement = document.createElement('script')
          }

          containerElement.innerHTML = e.responseText
          containerElement.id = uid

          // or: s[s.innerText!=undefined?"innerText":"textContent"] = e.responseText
          document.documentElement.appendChild(containerElement)
        }

        resolve(uid)

        /*
        s.addEventListener('load', function() {

        })
        */
      }, false)

      req.open('GET', what)
      req.send()
      // console.log('get', what)
    })
  }

  token() {
    return localStorage.token
  }

  waitForDOMReady(selector, delay = 5000) {
    return new Promise((resolve, reject) => {
      let checkInterval = setInterval(() => {
        let el = d3.select(selector)
        if (!el.empty()) {
          clearInterval(checkInterval)
          clearTimeout(timeout)
          resolve(el)
        }
      }, 50)

      let timeout = setTimeout(() => {
        clearInterval(checkInterval)
        reject(new Error('timeout for DOM element ' + selector))
      }, delay)
    })
  }

  /* get file url selecting between S3 and other endpoints */
  fileUrl(path, defaultPath) {
    if (!path) {
      return defaultPath
    } else if (!path.match(/api\/uploads\//)) {
      return path
    } else {
      return path + '&token=' + this.$utils.token()
    }
  }
}

var utilsPlugin = {
  install:
    function (Vue) {
      Vue.prototype.$utils = new UtilsService()
    }
}

export default utilsPlugin
