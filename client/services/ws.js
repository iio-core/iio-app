import Vue from 'vue'

import store from '../store'
import io from 'socket.io-client'

class WSService extends Vue {
  constructor() {
    super({ store })

    this.uuid = Math.random().toString(36).slice(2)

    this.socket = io.connect()

    /* this.socket.on('ws', data => {
      console.log(data)
    }) */
  }

  resetLocalCredentials() {
    localStorage.token = null
    this.$store.commit('user', null)
    this.socket._logged = false
    this.heartbeat = false
  }
}

var wsPlugin = {
  install:
    function (Vue) {
      Vue.prototype.$ws = new WSService()
    }
}

export default wsPlugin
