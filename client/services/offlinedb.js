import Vue from 'vue'
import store from '../store'
import loadjs from 'loadjs'

import initdb from './offline/initdb'

class OfflineDBService extends Vue {
  constructor() {
    super({ store })

    this.uuid = Math.random().toString(36).slice(2)

    this.init()
  }

  _checkSyncFilter(config) {
    return new Promise((resolve, reject) => {
      let filter = 'doc._id === "_design/sync_filter"'
      for (let collection of config.offline.collections) {
        filter += ' || doc.iio_collection === "' + collection + '"'
      }

      let f = (ff => {
        return 'function (doc) { return ' + ff + '}'
      })(filter)

      this.remote.get('_design/sync_filter').then(result => {
        if (result.statusCode && result.error) {
          this.remote.put({
            _id: '_design/sync_filter',
            filters: {
              all: f
            }
          }).then(pr => {
            console.log(pr)
            resolve()
          }).catch(perr => reject(perr))
        } else {
          console.log('design doc for sync already there', result)
          resolve()
        }
      }).catch(err => reject(err))
    })
  }

  async init() {
    let config = this.config = await this.$utils.getConfig()

    if (config.offline && config.offline.activated) {
      this.loadPouchDB().then(async PouchDB => {
        this.db = new PouchDB(config.offline.dbName)
        // console.log('offline remote db instantiated for ' +
        //   window.location.origin + '/api/sync/' + config.offline.dbName)
        this.remote =
          new PouchDB(window.location.origin + '/api/sync/' + config.offline.dbName)
        // console.log('offline remote db instantiated')

        await this._checkSyncFilter(config)

        console.log('sync filter activated')

        if (this.$store.state.user) this.onLogin()

        this.$services.$on('app:login', this.onLogin.bind(this))
        this.$services.$on('app:loggedout', this.onLogout.bind(this))

        initdb.call(this)
      }).catch(err => console.log(err))
    } else {
      console.log('offline db is not activated')
    }
  }

  loadPouchDB(version = '7.0.0') {
    return new Promise((resolve, reject) => {
      if (!loadjs.isDefined('PouchDB')) {
        loadjs('js/pouchdb-' + version + '.js',
          'PouchDB', {
            success: () => {
              loadjs('js/pouchdb.find.min.js', 'pouchdb.find', {
                success: () => {
                  resolve(global.PouchDB)
                },
                error: err => reject(err)
              })
            },
            error: err => reject(err)
          })
      } else {
        resolve(global.PouchDB)
      }
    })
  }

  dbInfo() {
    return new Promise((resolve, reject) => {
      this.db.info().then(result => {
        resolve(result)
      }).catch(function (err) {
        reject(err)
      })
    })
  }

  getUsers() {
    return new Promise((resolve, reject) => {
      this.db.find({ selector: { iio_collection: 'users' }}).then(results => {
        resolve(results)
      }).catch(function (err) {
        reject(err)
      })
    })
  }

  onLogin() {
    this.remote.replicate.to(this.db,
      {
        filter: 'sync_filter/all'
      },
      (err, response) => {
        if (err) {
          return console.log(err)
        }
        console.log(response)
      }
    )
  }

  onLogout() {
    console.log('offline db relication TO host started')
    this.db.replicate.to(
      this.remote,
      {
        filter: 'sync_filter/all'
      },
      (err, response) => {
        if (err) {
          return console.log(err)
        }
        console.log(response)
      }
    )
  }
}

var offlinedbPlugin = {
  install:
    function (Vue) {
      Vue.prototype.$offlinedb = new OfflineDBService()
    }
}

export default offlinedbPlugin
