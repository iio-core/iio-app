import * as d3 from 'd3'

export default function init() {
  this.$modules.waitForService('app').then(app => {
    d3.json('data/schemas/user.schema.json').then(doc => {
      app.generateDataFormJSONSchema({ schema: doc }).then(user => {
        user = _.cloneDeep(user.data)

        let item = {
          _id: "schema_users",
          data: user,
          schema: doc
        }

        this.db.put(item).then(result => {
          console.log('load offline data: users schema', result)
        }).catch(function (err) {
          console.log(err)
        })
      }).catch(err => console.log(err))
    }).catch(err => console.log(err))
  }).catch(err => console.log(err))
}
