# Ignitial.io application

_Ignitial.io application_ (iio-app) is the core web application skeleton for 
deploying web single page apps (SPA). It is the host for deploying serverless 
_Ignitial.io services_ (iios) in that meaning that it allows not only to access 
distributed features (as services or micro-services), but it also provides an 
integration mechanism for deploying full web apps within itself: in some way we 
are talking about apps in apps.  

## Installation

### Pre-requisites

Following information targets Linux distributions and more precisely Debian compliant ones.

Following apps need to be installed:
- if you run app on your host (and not as a Docker container), you need NodeJS to
be installed: [https://nodejs.org/](https://nodejs.org/)
- Docker: [https://docs.docker.com/install/linux/docker-ce/ubuntu/](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- Docker-compose: [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)
- Git:

```bash
sudo apt-get install git
```

If you are using provided _docker-compose.yml_ file for dependencies provisioning,
you need to create folowing directories:

> Note:  
> You can use _tools/helpers/prepare-dependencies.sh_ to produce following
> operations.

```bash
mkdir -p ~/iio-data
mkdir -p ~/iio-data/mongo 
mkdir -p ~/iio-data/minio
mkdir -p ~/iio-data/minio/config
mkdir -p ~/iio-data/minio/data
```

Then copy _minio_ configuration file:

```bash
# go to iio-app directory
cd iio-app
# cp file
cp tools/helpers/minio/config.json ~/iio-data/minio/config
```

In order to adapt to your context (and because docker-compose needs absolute paths),
you need to create a dedicated _docker-compose.yml_ file using the provided template.

```bash
# go to iio-app directory
cd iio-app
# generate compose file
cat template.docker-compose.yml | sed "s/_user_/$USER/g" > docker-compose.yml
```

## Get source code and NPM dependencies install

We suppose that you have configured SSH GitLab access.

```bash
# get code from GitLab
git clone git@gitlab.com:iio-core/iio-app.git
# go to iio-app directory
cd iio-app
# install NodeJS dependencies
npm install
```

## Populate DB

Make sure that the following lines are not commented in _tools/populate_db.js_:

```javascript
await roles(db)
await users(db)
await notifications(db)
```

as well as any other mandatory data collection.  
Then, run following script:

```bash
# go to iio-app directory
cd iio-app
# populate
tools/populate_db.js
```

## Starting in dev mode

We suppose that you have produced previous installation steps. Then:

```bash
# go to iio-app directory
cd iio-app
# start dependencies
cat template.docker-compose.yml | sed "s/_user_/$USER/g" > docker-compose.yml
```

## Main concepts  

### IIO services

IIOS are cloud native services or micro-services that can be accessed remotely 
thanks to a RPC-like mechanism.  

Each service has a back-end main entry point which is a class whose methods are 
exposed through the RPM mechanism. In order to do so, service entry class extends
__Service__ class from _iio-services_ package (this is a NodeJS package, but 
there is an equivalent to this in Python and Go).

Each exported method must return a __Promise__ (must be asynchronous if language
other than JS). Its parameters are:  
- _args_: an object containing method arguments as a dictionary
- _userId_: an eventually populated string that provides user's unique id when 
action has a user origin (from UI for example, but not only). This property is
tokenized through JWT  

For instance:

```javascript
class MyService extends Service {
  constructor(options) {
    super(options)
    ...
  }
  
  myServiceMethodOne(args) {
    return new 
  }
}
```

Take in account the fact that methods whose name starts width '_' or  '$' are not registered for remote call. In the same way, there are some reserved methods 
due to the automatic way of finding them. These methods are inherited (for 
example from NodeJS __EventEmitter__). See below the full list:

```javascript
const _RESERVED_METHODS = [
  '__defineGetter__',
  '__defineSetter__',
  '__lookupGetter__',
  '__lookupSetter__',
  'hasOwnProperty',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toLocaleString',
  'toString',
  'valueOf',
  'name',
  'addListener',
  'emit',
  'eventNames',
  'getMaxListeners',
  'listenerCount',
  'listeners',
  'on',
  'off',
  'once',
  'prependListener',
  'prependOnceListener',
  'removeAllListeners',
  'removeListener',
  'setMaxListeners',
  'rawListeners'
]
```

More details on how to build IIO services can be found [here](https://gitlab.com/iio-core/iio-services/wikis/home)

## Fronte-end side events management

There are mainly two ways to manage events in iio-app:
- through _$services_ Vue plugin: this allows to "plug" any event for internal use 
(could be external service as well: internal means here "within UI/front-end"). This allows as well to detect new services available;  
- through _app.ws.socket_ instance: this allows to manage push events coming from 
the back-end or from an external service (back-end side).  

### Events pluged to _$services_  

See below a list of basic events implemented in the base app template:  
- app:filter
- ui:dashboard:panel:resize
- app:help:contextual
- app:sidemenu:item:add
- app:sidemenu:item:remove
- app:notification
- app:context:buttons:add
- app:context:buttons:clear
- app:context:buttons:remove
- app:clipboard
- app:user:notification:delete
- app:user:notification:update
- app:login
- app:confirmation:request
- service:up
- service:down
