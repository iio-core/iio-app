#!/bin/sh

docker-compose -f docker-compose-couch.yml up -d mongo
sleep 1

./tools/populate_db-couch.js

docker-compose -f docker-compose-couch.yml stop mongo
docker-compose -f docker-compose-couch.yml rm -f mongo
