# IgnitialIO App

> Ignitial.io  
> IgnitialIO is a "Do everything" micro-services framework allowing to implement scale free complex platforms.  
> IgnitialIO App is its web server services portal, a services host application that that is just a simple SPA with functionalities available as containerized micro-services.

## Build & run

IIOApp is supposed to be used as a library that allows target application to keep itself up to date as per latest IIOApp versions.  

However, you can use it directly as a template, getting this repo and renaming as per your own app.  

If you do so, find below some basic commands to install and run.  

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run start:dev

# build for dev
npm run build:dev

# build for production with minification
npm run build:prod
```

### Local machine startup

For local (non Docker) startup, you should use a shell file to initializate main env variables.  
For example:

```bash
#!/bin/sh

docker start minio redis
sleep 1

export S3_SECURE=false
export S3_ENDPOINT=localhost
export S3_PORT=9000
export S3_ACCESS_KEY_ID=...
export S3_SECRET_ACESS_KEY=...

export IIO_SERVER_PORT=4093

export EMAILER_SMTP_PASS=toto

npm run start:dev
```

## Use as a library

If you want to use it as a library, you must install it in your own nodejs server context. The best way to do so is to use iio-app-template which allows you to bootstrap a full application.
