FROM node:8-alpine

RUN mkdir -p /opt && mkdir -p /opt/ignitialio

ADD . /opt/ignitialio

WORKDIR /opt/ignitialio

RUN npm install && npm run build:prod

CMD ["node", "./server/index.js"]
