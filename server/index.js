const serveStatic = require('serve-static')
const connect = require('connect')
const bodyParser = require('body-parser')
const Rest = require('connect-rest')
const multipart = require('connect-multiparty')
const http = require('http')
const path = require('path')

const defaultConfig = require('./config')

const AppManager = require('./core').AppManager

function Server(config, app, init) {
  config = config || defaultConfig
  let path2serve = path.join(process.cwd(), config.server.path)

  app = app || connect()
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(multipart())
    .use(serveStatic(path2serve, { 'index': [ 'index.html' ] }))

  let rest = Rest.create({
    context: config.rest.context,
    apiKeys: config.rest.apiKeys,
    logger: { level: 'error' }
  })

  // adds connect-rest middleware to connect
  app.use(rest.processRequest())

  const server = new http.Server(app)

  const appManager = new AppManager({
    server: server,
    config: config,
    rest: rest
  })

  // initialize modules
  // app init before base init
  if (init && typeof init === 'function') {
    init(appManager)
  }

  // base init
  appManager._init()

  // start web server
  server.listen(config.server.port, function(err) {
    if (err) { console.log(err) }
    console.log('Superstatically ready for ' + path2serve)
  })

  return appManager
}

if (require.main === module) {
  Server(defaultConfig)
} else {
  exports.Server = Server
}
