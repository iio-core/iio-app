const nodemailer = require('nodemailer')

const pino = require('../utils').pino.child({ origin: 'emailer' })

exports._name = 'emailer'

class Emailer {
  constructor(options) {
    this._options = options

    // Create a SMTP transporter object
    this._transporter = nodemailer.createTransport(options.smtp, options.mail)
  }

  send(mailOptions) {
    return new Promise((resolve, reject) => {
      this._transporter.sendMail(mailOptions, (err, info) => {
        if (err) {
          pino.error(err, 'Mailer failed')
          reject(err)
        } else {
          resolve(info)
        }
      })
    })
  }
}

exports._main = Emailer
