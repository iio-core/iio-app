const fs = require('fs')
const cpuStats = require('cpu-stats')
const { exec } = require('child_process')

const utils = require('../utils')
const pino = utils.pino.child({ origin: 'data' })

const Service = require('../services/core').Service
const Gateway = require('@ignitial/iio-services').Gateway

exports._name = 'data'

class Data extends Gateway {
  constructor(options) {
    options.name = 'data'
    super(options)

    // never registered
    this._dataServiceInfo = null

    // linsteners
    this._onIncomingEvent = this._processIncomingEvent.bind(this)
    this._onBridge = this._bridge.bind(this)
    this.on('service:registered', this._onBridge)

    this._init().then(() => {
      // manage heartbeat
      setInterval(() => {
        cpuStats(this._options.heartbeatPeriod || 5000, (error, result) => {
          this.$app.info().then(info => {
            this._emitHeartBeat({
              application: true,
              cpuLoad: result,
              err: error,
              ...info
            })
          }).catch(err => pino.error(err, 'failed to get app info'))
        })
      }, this._options.heartbeatPeriod || 5000)

      // insemination
      utils.waitForPropertyInit(this, 'roles', 20000).then(roles => {
        roles.isEmpty({}).then(result => {
          if (result.status) {
            exec('./tools/populate_db-' +
              this._options.engine + '.js', (err, stdout, stderr) => {
              if (err) {
                pino.error(err, 'failed to execute populate command')
              } else {
                pino.warn('populate done since empty on start')
                console.log(stdout, stderr)
                this.services[this._options.service].restart().then(() => {
                  pino.warn('restarting data lake service...')
                }).catch(err => {
                  pipo.error(err, 'fail to restart data lake')
                })
              }
            })
          } else {
            pino.warn('db already populated')
          }
        }).catch(err => {
          pino.error(err, 'insemination failed')
        })
      }).catch(err => pino.error('roles datum not available'))
    }).catch(err => console.log(err))
  }

  _bridge(service) {
    if (service.name !== this._options.service) return
    if (this._dataServiceInfo) {
      if (service.creationTimestamp >= this._dataServiceInfo.creationTimestamp) {
        this._dataServiceInfo = service
        pino.warn('data service has been overwritten')
        this.ctrledRegister()
      } else {
        pino.warn('data service already registered')
      }
    } else {
      this._dataServiceInfo = service
      pino.info('data service registered')
      this.ctrledRegister()
    }
  }

  async _processIncomingEvent(event) {
    if (!!event.topic.match(/module\:data\:?.*\:request/)) {
      pino.info('data request', event)
      let datum = event.topic.split(':')[2]
      let topic = 'module:' + this._name + ':' + datum + ':' + event.method
        + ':' + event.token

      // injects userid for authorization check as per user's roles
      // injects logged info
      let loggedUser = this.$app.ws.clients[event.source].socket._logged

      // injects userid for authorization check as per user's roles and call
      // service method
      // 2018/08/15: detokenize userID
      let decoded = {}
      try {
        decoded = await this.$app.$data.users.checkToken({ token: event.userId })
        decoded = decoded || {}
      } catch (err) {
        pino.warn('data service method ' + event.method + ' token check failed: ' + err)
      }

      this._services[this._options.service][datum][event.method]
        (event.args, decoded._id, loggedUser).then( result => {
          pino.info('[%s] -> response [%s] - user [%s]',
            this._name, topic, decoded._id)

          this.$app.ws.clients[event.source].socket.emit(topic, {
            result: result
          })
        }).catch(err => {
          this.$app.ws.clients[event.source].socket.emit(topic, { err: '' + err })

          pino.error(err, 'data service method ' + event.method + ' call failed')
        })
    }
  }

  ctrledRegister() {
    if (this.$app.rootServices[this._name]) {
      pino.warn('data service already registered')
      this.$app.ws.off('module:event', this._onIncomingEvent)
    }

    if (!this._dataServiceInfo) {
      pino.warn('remote service not available yet, registration delayed')
      return
    }

    this.$app.ws.on('module:event', this._onIncomingEvent)

    try {
      // aliases: namespace === datum
      for (let ns in this._services[this._options.service]) {
        this[ns] = this._services[this._options.service][ns]
      }

      // defines sub-services information
      let subs = {}

      if (this._dataServiceInfo && this._dataServiceInfo.methods) {
        for (let s of this._dataServiceInfo.methods) {
          let service = s.split(':')[0]
          let method = s.split(':')[1]
          subs[service] = subs[service] || []
          subs[service].push(method)
        }
      }

      // service discovery: add info to rootService table that can be obtained
      // from client side
      this.$app.rootServices[this._name] = {
        name: this._name,
        methods: null,      // means that only sub-services methods are available
        subs: subs          // sub-services declaration
      }
    } catch (err) {
      pino.error(err)
    }
  }
}

exports._main = Data
