const _ = require('lodash')
const EventEmitter = require('events').EventEmitter

const uuid = require('../../utils').uuid
const pino = require('../../utils').pino.child({ origin: 'services' })

const getAllMethods = require('../../utils').getAllMethods

/* Unified service base class */
class Service extends EventEmitter {
  constructor(io, options) {
    super()

    try {
      this._options = options || {}

      // reference to the client WS
      this._io = io

      // service name
      this._name = this._options.name || Math.random().toString(36).slice(2)

      // service public methods
      this._methods = []

      // public options send to client
      this._publicOptions = this._options.publicOptions

      // sets an unique id for the service
      this.uuid = uuid()

      // detect methods to be exposed as per design rules
      this._detectMethods()
    } catch (err) {
      pino.error(err, 'Falied to build service. Exiting...', this._name)
      process.exit(1)
    }

    pino.info('[' + this._name + '] has been defined with uuid= ' + this.uuid)
  }

  get name() {
    return this._name
  }

  async _checkAccess(method, userId) {
    return new Promise((resolve, reject) => {
      this.$app.$data.users.get({ _id: userId }).then(user => {
        if (user) {
          this.$app.$data.roles.get({}).then(roles => {
            if (roles[user.role] && roles[user.role][this._name]) {
              if (_.find(roles[user.role][this._name].exec, e => { return e === method })) {
                resolve()
              } else {
                reject(new Error('service ' + this._name + ' access not granted for role ' + user.role))
              }
            } else {
              resolve() // default behaviour unsecured since double check with data access
            }
          }).catch(err => {
            pino.error(err, 'failed to fetch roles')
            reject(new Error('roles not defined'))
          })
        } else {
          this.$app.$data.roles.get({}).then(roles => {
            if (roles.anonymous) {
              if (_.find(roles.anonymous.exec, e => { return e === method })) {
                resolve()
              } else {
                reject()
              }
            } else {
              resolve() // default behaviour unsecured since double check with data access
            }
          }).catch(err => {
            pino.error(err, 'failed to fetch roles')
            reject(new Error('roles not defined'))
          })
        }
      }).catch(err => {
        pino.warn('failed to fetch user ' + userId + ' with err: ' + err)
        this.$app.$data.roles.get({}).then(roles => {
          if (roles.anonymous && roles.anonymous.exec) {
            if (_.find(roles.anonymous.exec, e => { return e === method })) {
              resolve()
            } else {
              reject()
            }
          } else {
            resolve() // default behaviour unsecured since double check with data access
          }
        }).catch(err => {
          reject(new Error('roles not defined'))
        })
      })
    })
  }

  /* registers public method */
  _addMethod(name, fct) {
    this._methods.push(name)
    this[name] = fct
  }

  /* detects public methods as per the naming rules */
  _detectMethods() {
    let properties = getAllMethods(this)

    for (let p of properties) {
      this._methods.push(p)
    }
  }

  /* register service logic for methods remote call through WS socket */
  _register() {
    pino.info('[%s] methods registration [%s] with public options [%s]',
      this.name, this._methods, this._publicOptions)

    // on service method request
    this._io.on('service:' + this._name + ':request', async data => {
      try {
        let topic = 'service:' + this._name + ':' + data.method + ':' + data.token

        // prohibited methods:
        // _ -> internal/private
        // $ -> injected
        if (data.method[0] === '_' && data.method[0] === '$') {
          this._io.emit(topic, {
            err: 'private method call not allowed'
          })

          pino.warn('private method [%s] call not allowed for service [%s]', data.method,
            this._name)

          return
        }

        // 2018/08/15: detokenize userID
        let decoded = {}
        try {
          decoded = await this.$app.$data.users.checkToken({ token: data.userId })
          decoded = decoded || {}
        } catch (err) {
          pino.warn('data service method ' + data.method + ' token check failed: ' + err)
        }

        // TBD: finalize new access concepts
        try {
          await this._checkAccess(data.method, decoded._id)
        } catch (err) {
          pino.error(err, 'UNAUTHORIZED service [%s] method [%s] call: needs autorized user',
            this._name, data.method)

          this._io.emit(topic, {
            err: 'service ' + this._name + ' needs authorized user'
          })

          return
        }

        pino.info('[%s]-> request [%s] - instance: [%s], user: [%s]',
          this._name, data.method, this.uuid, decoded._id)

        if (this[data.method]) {
          // injects userid for authorization check as per user's roles
          this[data.method](data.args, decoded._id).then(result => {
            pino.info('[%s]-> response [%s] - user [%s]',
              this._name, topic, decoded._id)

            this._io.emit(topic, {
              result: result
            })
          }).catch(err => {
            this._io.emit(topic, { err: err + '' })
          })
        } else {
          this._io.emit(topic, {
            err: 'Method [' + data.method + '] is not available for service [%s]' + this._name
          })
        }
      } catch (err) {
        pino.error(err, 'failed to call method [%s] for service [%s]', data.method, this._name)
      } // try/catch
    })

    // tells client that service is up
    this._io.emit('service:up', {
      name: this._name,
      methods: this._methods,
      options: this._publicOptions
    })
  }

  /* unregister service and inform client that service is down */
  _unregister() {
    pino.info('[%s] unregistration', this.name)

    try {
      this._io.removeAllListeners('service:' + this._name + ':request')

      // tells client that service is down
      this._io.emit('service:down', {
        name: this._name
      })
    } catch (err) {
      pino.error(err, 'failed to unregister', this._name)
    }
  }
}

exports.Service = Service

/**
 Services manager
**/
class Services extends Service {
  constructor(io, options) {
    super(io, {
      name: 'services',
      ...options
    })

    // services references
    this._services = {}

    this._register()
  }

  /* register and instantiates a new service class: not exported */
  _registerService(ServiceClass, opts) {
    let service = new ServiceClass(this._io, opts)

    this._services[service.name] = service

    pino.info('service [' + service.name + '] registered')
  }

  /* unregister and "uninstantiates" a service class: not exported */
  _unregisterService(service) {
    if (typeof service === 'object') {
      if (this._services[service.name]._destroy) {
        this._services[service.name]._destroy()
      }

      delete this._services[service.name]
    } else if (typeof service === 'string') {
      if (this._services[service]._destroy) {
        this._services[service]._destroy()
      }

      delete this._services[service]
    }
  }

  /* unregister all services: not exported */
  _unregisterAll() {
    for (let s in this._services) {
      this._unregisterService(s)
    }
  }
}

exports.Services = Services
