const httpRequest = require('http_request')

const Service = require('../core').Service

class Geodata extends Service {
  constructor(io, options) {
    super(io, {
      name: 'geodata',
      ...options
    })

    this._register()
  }

  reverse(args) {
    // @_GET_
    return new Promise((resolve, reject) => {
      let baseURL = 'http://nominatim.openstreetmap.org/reverse'
      let params = 'format=jsonv2&lat=' + args.lat + '&lon=' + args.lon
      let url = baseURL + '?' + params

      httpRequest.post(url, {}).then(response => {
        if (response.code !== 200) {
          reject(response)
        } else {
          let result = JSON.parse(response.body)

          resolve({
            location: {
              lat: args.lat,
              lon: args.lon
            },
            address: result.address,
            name: result.name
          })
        }
      })
    })
  }

  location(args) {
    // @_GET_
    let address = args.address

    return new Promise((resolve, reject) => {
      let baseURL = 'http://nominatim.openstreetmap.org/search/'
      let params = 'format=jsonv2'
      let url = baseURL + address + '?' + params

      httpRequest.post(url, {}).then(function(response) {
        if (response.code !== 200) {
          reject(response)
        } else {
          var result = JSON.parse(response.body)[0]
          if (result) {
            resolve({
              lat: result.lat,
              lon: result.lon
            })
          } else {
            reject(new Error('not found'))
          }
        }
      })
    })
  }
}

module.exports = Geodata
