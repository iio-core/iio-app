const fs = require('fs')
const stream = require('stream')

const req = require('request')

const Service = require('../core').Service
const Gateway = require('@ignitial/iio-services').Gateway

const pino = require('../../utils').pino.child({ origin: 'apigateway' })

class APIGateway extends Service {
  constructor(io, options) {
    super(io, {
      name: 'apigateway',
      ...options
    })

    // maps dynamic service on dynamic methods for each service available through
    // gateway
    this._bridges = {}

    // listeners
    this._listeners = {}

    // maintains services info trough IIOS gateway
    this._gateway = new Gateway(this._options)

    this._gateway._init().then(() => {
      this._reset().then(() => {
        // service up
        this._listeners.service = this._createBridge.bind(this)
        this._gateway.on('service:registered', this._listeners.service)

        // service deletion: one by one
        this._listeners.deleted = this._cleanUpBridges.bind(this)
        this._gateway.on('service:unregistered', this._listeners.deleted)

        // manage service specific publications
        this._listeners.event = this._eventHandler.bind(this)
        this._gateway._subscribePushEvent('*', this._listeners.event)

        // heartbeats...
        this._gateway._subscribeHeartBeat()
        this._gateway.on('heartbeat', data => {
          this._io.emit('service:heartbeat:event', data)
        })

        this.$app._rest.get('/services/:service/*filename',
          async (request, content) => {
            try {
              // grab original service info
              let service =
                (await this._gateway.getServiceInfo(request.parameters.service)).info

              let url = 'http://' + service.options.metadata.host +
                ':' + service.options.metadata.port + '/' +
                request.parameters.filename

              function _request(url) {
                return new Promise((resolve, reject) => {
                  req(url, (err, response, body) => {
                    if (err) {
                      reject(err)
                    } else {
                      resolve(body)
                    }
                  })
                })
              }

              let content = await _request(url)

              return content
            } catch (err) {
              return err
            }
          }, { contentType: 'text/html' })

        this.$app._rest.get('/images/:service/*filename', async (request, content) => {
          try {
            // grab original service info
            let service = (await this._gateway.getServiceInfo(request.parameters.service)).info

            let url = 'http://' + service.options.metadata.host +
              ':' + service.options.metadata.port + '/' +
              request.parameters.filename

            let reqImg = uri => {
              return new Promise((resolve, reject) => {
                req({
                  uri: uri,
                  method: 'GET',
                  encoding: null
                }, (err, response, body) => {
                  if (err) {
                    reject(err)
                  } else {
                    try {
                      let data = new stream.Readable()
                      // needs to be defined
                      data._read = function(n) {}
                      data.push(body)
                      resolve(data)
                    } catch (err) {
                      reject(err)
                    }
                  }
                })
              })
            }

            let response = await reqImg(url)
            return response
          } catch (err) {
            pino.error(err, 'failed to get image file')
            return err
          }
        })

        // register apigateway as an unified service itself
        this._register()
      }).catch(err => {
        pino.error(err, 'APIGateway reset failed')
      })
    }).catch(err => {
      pino.error(err, 'APIGateway initialization failed')
    })
  }

  // creates bridge for given service
  _createBridge(service) {
    // remote service already registered
    if (this._bridges[service.name]) {
      pino.warn('duplicate registration for service [%s]', service.name)
      return
    }

    this._gateway._waitForService(service.name).then(() => {
      try {
        pino.info(service, 'registering new service [%s] by gateway',
          service.name, this.uuid)

        // avoid initial duplication: TBD => improvements expected
        if (this._bridges[service.name] &&
          this._bridges[service.name].$creationTimestamp >= service.creationTimestamp) {
          pino.warn('duplicate registration for service [%s]', service.name)
          return
        } else if (this._bridges[service.name]) {
          pino.warn('duplicate service [%s] will be overwritten', service.name)
        }

        // ...when done, creates new unified service bridge
        this._bridges[service.name] = new Service(this._io, {
          name: service.name,
          publicOptions: service.options
        })

        // inject creation timestamp property for further checks
        this._bridges[service.name].$creationTimestamp = service.creationTimestamp

        for (let method of service.methods) {
          // create method call bridge in a standard way: no additional processing
          this._bridges[service.name]
            ._addMethod(method, this._gateway.services[service.name][method])
        }

        // register full service as a gateway bridged object
        this._bridges[service.name]._register()
      } catch (err) {
        pino.error(err, 'failed when registering service')
      }
    }).catch(err => {
      pino.error(err, '- service ' + service.name + ' up, but timeout on initialization')
    })
  }

  /* resets gateway reloading all data */
  _reset() {
    return new Promise((resolve, reject) => {
      this._bridges = {}

      // create bridges for already detected services
      this._gateway._getAvailableNSServices().then(services => {
        for (let serviceName in services) {
          this._createBridge(services[serviceName])
        }

        pino.info({ options: this._options }, 'api gateway created')
        resolve()
      }).catch(err => reject(err))
    })
  }

  /* handle service specific event */
  _eventHandler(data) {
    // push to client
    this._io.emit('service:' + data.service + ':event:' + data.name, data)
  }

  /* clean up all bridges or selected subset only */
  _cleanUpBridges(subset) {
    // destroy and unregister each single bridged service
    for (let s in this._bridges) {
      if (subset) {
        if (subset.indexOf(s) >= 0) {
          this._bridges[s]._unregister()

          delete this._bridges[s]

          pino.warn('destroy [%s] bridge from gateway [%s]', s, this.uuid)
        }
      } else {
        this._bridges[s]._unregister()

        delete this._bridges[s]

        pino.warn('destroy [%s] bridge from gateway [%s]', s, this.uuid)
      }
    }
  }

  /* kind of destructor */
  _destroy() {
    try {
      this._gateway._unsubscribeHeartBeat()
      this._gateway.removeAllListeners()
      this._cleanUpBridges()
      this._unregister()

      pino.warn('listeners count', this._gateway.listenerCount('service'))
      pino.warn('destroying ext gateway [%s]', this._gateway.uuid)
      this._gateway._destroy()
      pino.warn('apigateway [%s] destroyed', this.uuid)
    } catch (err) {
      pino.error('failed to destroy api gateway')
    }
  }

  reset() {
    return new Promise((resolve, reject) => {
      try {
        pino.info('reset requested from remote for gateway [%s]', this.uuid)

        this._destroy()
        this._reset().then(() => {
          resolve()
        }).catch(err => reject(err))
      } catch (err) {
        pino.error(err, 'reset err')

        reject(new Error('gateway reset failed'))
      }
    })
  }

  /* get Redis keys related to namespace or not*/
  redisKeys(args, userId) {
    return this._gateway.redisKeys(args, userId)
  }

  /* delete Redis key */
  redisDelKey(args, userId) {
    return this._gateway.redisDelKey(args, userId)
  }

  /* delete Redis key */
  redisGet(args, userId) {
    return this._gateway.redisGet(args, userId)
  }
}

module.exports = APIGateway
