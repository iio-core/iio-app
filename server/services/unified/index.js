const fs = require('fs')
const path = require('path')

const pino = require('../../utils').pino.child({ origin: 'unified' })

module.exports = {}

for (let f of fs.readdirSync(__dirname)) {
  let basename = path.basename(f, '.js')
  if (basename !== 'index' && path.extname(f) !== '.map') {
    pino.info('registering unified services for file [%s]', f)

    let m = require('./' + f)
    module.exports[basename] = m
  }
}
