const path = require('path')
const fs = require('fs')
const os = require('os')
const EventEmitter = require('events').EventEmitter
const Minio = require('minio')
const empty = require('json-schema-empty').default
const urlencode = require('urlencode')
const _ = require('lodash')
const httpRequest = require('request-promise-native')

const WSManager = require('./ws').WSManager
const Service = require('../services/core').Service

const utils = require('../utils')
const serverModules = require('../modules')

const pino = require('../utils').pino.child({ origin: 'core' })

class AppManager extends EventEmitter {
  constructor(options) {
    super()

    this._server = options.server
    this._config = options.config
    this._rest = options.rest

    // inject app instance to any core object
    Service.prototype.$app = this
    WSManager.prototype.$app = this

    // Current Working Directory from app configuration
    this._cwd = path.join(process.cwd(), this._config.server.path)

    // managers
    this.ws = new WSManager(this._server, this._config)

    // modules that provide root services
    this.rootServices = {}

    // manage file uploads
    this._minioClient = new Minio.Client(this._config.store.minio)

    let bucket = this._config.store.bucket.name
    let region = this._config.store.bucket.region

    // not to be used with S3 due to endpoint creation delays
    this._minioClient.bucketExists(bucket, (err, exists) => {
      if (!err && !exists) {
        pino.warn('Bucket ' + bucket + ' does not exist')

        this._minioClient.makeBucket(bucket, region, errOnMake => {
          if (errOnMake) {
            pino.error(errOnMake, 'Bucket ' + bucket + ' cannot be created')
          } else {
            pino.info('Bucket [%s] has been created', bucket)
          }
        })
      } else if (err) {
        pino.error('Socket hangup when connecting to S3/Minio')
        process.exit(1)
      }
    })

    // REST service for uploaded files
    let getObject = request => {
      return new Promise((resolve, reject) => {
        this._minioClient.getObject(bucket,
          urlencode.decode(request.parameters.userid + '/' + request.parameters.filename),
          (err, readableStream) => {
            if (err) {
              reject(err)
            } else {
              resolve(readableStream)
            }
          })
      })
    }

    this._rest.get('/uploads/:userid/:filename', async (request, content) => {
      let stream = null
      try {
        await this._checkRESTAccess(request.parameters.token)
        stream = await getObject(request)
      } catch (err) {
        stream = err
        pino.error(err, 'failed getting file', request.parameters)
      }

      return stream
    })

    this._rest.post('/upload', async (request, content) => {
      return 'ok'
    })

    this._rest.post('/dropfiles', async (request, content) => {
      let params = request.paramaters
      // console.log(request.files)
      try {
        if (request.files) {
          if (!utils.fileExists(this._config.server.filesDropPath)) {
            fs.mkdirSync(this._config.server.filesDropPath)
          }

          let files = Object.values(request.files)
          if (!Array.isArray(files)) files = [ files ]

          for (let f of files) {
            let filename = path.basename(f.name)
            try {
              let dest = path.join(this._config.server.filesDropPath, filename)

              let readStream = fs.createReadStream(f.path)
              let writeStream = fs.createWriteStream(dest)
              readStream.pipe(writeStream)

              /* writeStream.on('end', () => {
                // console.log('---> end writeStream')
              }) */

              writeStream.on('error', err => {
                pino.error(err, 'drop failed')
              })
            } catch (err) {
              pino.error(err, 'Failed to drop file')
            }
          }
          // console.log('RRR---', files)
          return files
        } else {
          pino.warn(request.files, 'File drop request but empty data')

          return 'empty'
        }
      } catch (err) {
        return 'ko'
      }
    })

    // Health API endpoint
    this._rest.get('/healthcheck', async (request, content) => {
      let healthInfo = await this.info()
      healthInfo.hostname = os.hostname()
      healthInfo.cpus = os.cpus()
      healthInfo.loadavg = os.loadavg()
      return healthInfo
    })

    // sync
    if (this._config.offline.activated) {
      this._rest.get({ path: '/sync/*', unprotected: true },
        async (request, content) => {
          try {
            let url = this._config.offline.uri +
              request.headers.originalUrl.replace('/api/sync/', '')
            // console.log('GET', url)
            let response = JSON.parse(await httpRequest.get(url))
            // console.log('GET res', response)

            return response
          } catch (err) {
            // console.log('GET err', err)
            return err
          }
        })

      this._rest.post({ path: '/sync/*', unprotected: true },
        async (request, content) => {
          try {
            let url = this._config.offline.uri +
              request.headers.originalUrl.replace('/api/sync/', '')
            // console.log('POST', url, request.body)
            let response = await httpRequest.post(url, {
              body: request.body,
              json: true
            })

            // console.log('POST res', response)

            return response
          } catch (err) {
            // console.log('POST err', err)
            return err
          }
        })

      this._rest.put({ path: '/sync/*', unprotected: true },
        async (request, content) => {
          try {
            let url = this._config.offline.uri +
              request.headers.originalUrl.replace('/api/sync/', '')
            // console.log('PUT', url, request.body)
            let response = await httpRequest({
                method: 'PUT',
                uri: url,
                body: request.body,
                json: true
              })
            // console.log('PUT res', response)

            return response
          } catch (err) {
            // console.log('PUT err', err)
            return err
          }
        })

      this._rest.del({ path: '/sync/*', unprotected: true },
        async (request, content) => {
          try {
            let url = this._config.offline.uri +
              request.headers.originalUrl.replace('/api/sync/', '')
            // console.log('DEL', url)
            let response = JSON.parse(await httpRequest.del(url))

            return response
          } catch (err) {
            return err
          }
        })
    }

    // all done
    pino.info('Application manager created')
  }

  _init() {
    // load & instantiate modules
    for (let _name in serverModules) {
      if (_name) {
        this._useModule(_name, serverModules[_name])
      }
    }

    this.ws.on('module:event', async event => {
      if (!!event.topic.match('module:app:request')) {
        let topic = 'module:app:' + event.method + ':' + event.token

        // prohibited methods:
        // _ -> internal/private
        // $ -> injected
        if (event.method[0] !== '_' && event.method[0] !== '$') {
          // injects userid for authorization check as per user's roles
          // injects logged info
          let loggedUser = this.ws.clients[event.source].socket._logged

          // 2018/08/15: detokenize userID
          let decoded = {}
          try {
            decoded = await this.$data.users.checkToken({ token: event.userId })
            decoded = decoded || {}
          } catch (err) {
            pino.warn('app token check failed when answering [%s] method: [%s]', event.method, err)
          }

          this[event.method](event.args, decoded._id, loggedUser).then(result => {
            pino.info('app -> response [%s] - user [%s]', topic, decoded._id)

            this.ws.clients[event.source].socket.emit(topic, {
              result: result
            })
          }).catch(err => {
            this.ws.clients[event.source].socket.emit(topic, err)
            pino.error('app failed when answering [%s] method with error', event.method, err)
          })
        } else {
          this.ws.clients[event.source].socket.emit(topic, {
            err: 'method not allowed: ' + event.method
          })

          pino.error('method %s not allowed for app', event.method)
        }
      }
    })

    pino.info('Application manager initialized')
  }

  /* use (instantiate) module dynamically using _require_ */
  _useModule(name, ModClass) {
    if (this['$' + name]) {
      throw new Error('Module name conflict for ' + name)
    }

    // inject app reference to module
    ModClass.prototype.$app = this

    // create module instance and pass configuration
    this['$' + name] = new ModClass(this._config.modules[name])

    // call init function if any
    if (typeof this['$' + name]._init === 'function') {
      this['$' + name]._init()
    }

    // is root service and needs to be added to the root services list
    if (typeof this['$' + name]._register === 'function') {
      this['$' + name]._register()
    }

    pino.info('[' + name + '] module loaded')
  }

  /* check REST access rights */
  _checkRESTAccess(token) {
    return new Promise((resolve, reject) => {
      this.$data.users.checkToken({ token: token })
        .then(() => resolve())
        .catch(err => reject(err))
    })
  }

  /* generate data from JSON Schema */
  generateDataFormJSONSchema(args) {
    return new Promise(async (resolve, reject) => {
      try {
        if (typeof args.schema === 'string') {
          let schema = this._config.domain.schemas[args.schema]
          if (schema) {
            let json = empty(schema)

            resolve({ data: json })
          } else {
            pino.error('failed to generate data')
            reject(new Error('failed to generate data'))
          }
        } else {
          let json = empty(args.schema)
          resolve({ data: json })
        }
      } catch (err) {
        pino.error(err, 'failed to generate data')
        reject(new Error('failed to generate data'))
      }
    })
  }

  /* provide modules list to remote (only root sevices remotely available) */
  modules() {
    return new Promise((resolve, reject) => {
      resolve({
        list: this.rootServices
      })
    })
  }

  /* provide app info */
  info() {
    return new Promise((resolve, reject) => {
      if (!this.appInfo) {
        fs.readFile('package.json', 'utf8', (err, result) => {
          if (err) {
            reject(err)
          } else {
            this.appInfo = {
              name: JSON.parse(result).name,
              version: JSON.parse(result).version
            }
            resolve(this.appInfo)
          }
        })
      } else {
        resolve(this.appInfo)
      }
    })
  }

  /* provide REST API keys */
  restAPIKeys() {
    return new Promise((resolve, reject) => {
      resolve({ keys: this._config.rest.apiKeys })
    })
  }
}

exports.AppManager = AppManager
