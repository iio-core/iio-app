'use strict'

const fs = require('fs')
const path = require('path')

const EventEmitter = require('events').EventEmitter
const IO = require('socket.io')
const SocketStream = require('socket.io-stream')

const utils = require('../utils')
const pino = utils.pino.child({ origin: 'ws' })

const Service = require('../services/core').Service
const Services = require('../services/core').Services

const _unifiedServices = require('../services/unified')

class WSManager extends EventEmitter {
  constructor(server, config) {
    super()

    if (!config) {
      pino.info('Come on, dumber, a small configuration is easy stuff...')
      process.exit()
    }

    this._config = config

    // WS server
    this._io = IO(server)

    // WS server clients
    this._clients = {}

    // inject WS reference into services
    Service.prototype.$ws = this

    this._io.on('connection', socket => {
      pino.info('new client connection', socket.client.conn.remoteAddress)

      // manage file streams through minio/S3
      SocketStream(socket).on('ws:file:upload', (stream, data) => {
        if (data && data.name) {
          let filename = path.basename(data.name)
          try {
            this.$app._minioClient.putObject(data.bucket, data.userId + '/' + filename, stream,
              data.size, (err, etag) => {
                if (err) {
                  socket.emit('ws:file:upload:result', {
                    err: '' + err
                  })
                  pino.error(err, 'S3 upload failed')
                } else {
                  data.etag = etag
                  socket.emit('ws:file:upload:result', data)
                }
              })
          } catch (err) {
            pino.error(err, 'Minio client failed to put file')
          }
        } else {
          pino.warn(data, 'File upload request but empty data')
        }
      })

      // manage file drop through streams
      SocketStream(socket).on('ws:file:drop', (stream, data) => {
        if (data && data.folder && data.name) {
          let filename = path.basename(data.name)
          try {
            if (!utils.fileExists(this._config.server.filesDropPath)) {
              fs.mkdirSync(this._config.server.filesDropPath)
            }

            let folder = path.join(this._config.server.filesDropPath,
              data.folder)
            data.dest = path.join(folder, filename)

            if (!utils.fileExists(folder)) {
              fs.mkdirSync(folder)
            }

            let writeStream = fs.createWriteStream(data.dest)
            stream.pipe(writeStream)

            writeStream.on('end', () => {
              socket.emit('ws:file:drop:result', data)
              // console.log('end writeStream')
            })

            writeStream.on('error', err => {
              socket.emit('ws:file:drop:result', {
                err: '' + err
              })

              pino.error(err, 'drop failed')
            })
          } catch (err) {
            pino.error(err, 'Failed to drop file')
          }
        } else {
          pino.warn(data, 'File drop request but empty data')
        }
      })

      try {
        // ensure root services (modules) work
        socket.on('module:event', event => {
          if (socket.client) {
            // inject source info
            event.source = socket.client.id

            // emits normalized event for any client events
            this.emit('module:event', event)
          }
        })

        // new client registered locally
        this._clients[socket.client.id] = {
          socket: socket,
          services: null
        }

        // tells app that a new client is there
        this.emit('client', socket.client.id)

        socket.on('disconnect', () => {
          this._clients[socket.client.id].services._unregisterAll()
          delete this._clients[socket.client.id].services
          delete this._clients[socket.client.id]

          pino.warn('deleted services for ' + socket.client.id)
        })

        // new services manager for client WS socket
        this._clients[socket.client.id].services =
          new Services(socket, this._config.unified.options['services'])

        // loads available unified services declared in unified index file
        for (let s in _unifiedServices) {
          this.addService(s,
            _unifiedServices[s],
            this._config.unified.options[s],
            socket.client.id)
        }

        // just for debugging client side
        socket.emit('ws', { status: 'ready' })

        setInterval(() => {
          socket.emit('heartbeat')
        }, 3000)
      } catch (err) {
        pino.error(err, 'something weird happened')
      }
    })

    pino.info('WSManager ready')
  }

  /* IO server */
  get io() {
    return this._io
  }

  /* clients list */
  get clients() {
    return this._clients
  }

  /* add unified service */
  addService(name, service, options, clientId) {
    if (this._clients[clientId]) {
      utils.waitForPropertyInit(this._clients[clientId], 'services').then(services => {
        services._registerService(
          service,
          {
            name: name,
            ...options
          })
      }).catch(err => {
        pino.warn(err, 'add service failed for client ' + clientId)
      })
    }
  }

  /* get service called serviceName for a given client */
  getService(clientId, serviceName) {
    return this._clients[clientId] ? this._clients[clientId].services._services[serviceName] : null
  }
}

exports.WSManager = WSManager
