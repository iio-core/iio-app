const AUTH_SECRET = process.env.AUTH_SECRET || 'Once upon the time, for ever'

const IIO_SERVER_PORT =
  process.env.NODE_ENV === 'production' ? 8080
    : (process.env.IIO_SERVER_PORT ? parseInt(process.env.IIO_SERVER_PORT) : 4093)

const REST_API_KEY = '849b7648-14b8-4154-9ef2-8d1dc4c2b7e9'

module.exports = {
  server: {
    port: IIO_SERVER_PORT,
    path: './dist',
    filesDropPath: './dist/dropped'
  },
  rest: {
    context: '/api',
    apiKeys: [ REST_API_KEY ],
    _unified: true
  },
  logout: {
    timeout: 15  /* minutes */,
    _unified: true
  },
  store: require('./store'),
  modules: require('./modules'),
  i18n: require('./i18n'),
  unified: require('./unified'),
  domain: require('./domain'),
  appMenu: require('./appmenu'),
  theming: {
    flatToolBar: false,
    toolbarColor: 'white',
    darkTheme: false,
    _unified: true
  },
  offline: {
    activated: false,
    pouchdbVersion: '7.0.0',
    uri: process.env.OFFLINEDB_URI || 'http://127.0.0.1:5984/',
    dbName: process.env.DB_NAME || 'ignitialio',
    collections: [ 'users' ],
    _unified: true
  }
}
