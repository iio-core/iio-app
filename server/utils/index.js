const fs = require('fs')
const promisify = require('util').promisify

function _promisifyAllFunctions (object) {
  for (const key of getAllMethods(object)) {
    const func = object[key]
    if (typeof func === 'function') {
      object[`${key}Async`] = promisify(func)
    }
  }
}

exports.promisifyAll = function (object) {
  _promisifyAllFunctions(object)

  const proto = Object.getPrototypeOf(object)
  if (proto) {
    _promisifyAllFunctions(proto)
  }

  return object
}

exports.fileExists = function(path) {
  try {
    fs.accessSync(path)
    return true
  } catch (err) {
    return false
  }
}

let getAllMethods = exports.getAllMethods
  = require('@ignitial/iio-services/lib/util').getMethods

let pino = require('pino')
let pretty

if (process.env.NODE_ENV !== 'production') {
  pretty = pino.pretty()
  pretty.pipe(process.stdout)
}

exports.pino = pino({
  name: 'iio-app',
  safe: true,
  level: 'warn'
}, pretty)

exports.uuid = () => {
  return Math.random().toString(36).slice(2)
}

/* wait for obj property to be defined */
exports.waitForPropertyInit = (obj, name, delay) => {
  delay = delay || 5000

  return new Promise((resolve, reject) => {
    let checkTimeout

    let checkInterval = setInterval(() => {
      if (obj[name]) {
        clearInterval(checkInterval)
        clearTimeout(checkTimeout) // nothing if undefined

        resolve(obj[name])
      }
    }, 100)

    checkTimeout = setTimeout(() => {
      if (checkInterval) {
        clearInterval(checkInterval)
        reject(new Error('timeout: [' + name + '] property is not available'))
      }
    }, delay)
  })
}
