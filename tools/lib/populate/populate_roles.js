const rolesData = require('../../data/roles')

exports.populate = async (db, resetOnly) => {
  let roles = db.collection('roles')

  await roles.deleteMany({}) // reset
  if (resetOnly) return

  await roles.insertOne(rolesData, { w: 1 })
}
