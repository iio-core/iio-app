#!/bin/sh

echo "Create mandatory folders..."
mkdir -p ~/iio-data
mkdir -p ~/iio-data/mongo
mkdir -p ~/iio-data/minio
mkdir -p ~/iio-data/minio/config
mkdir -p ~/iio-data/minio/data

echo "Copy Minio configuration..."
cp tools/helpers/minio/config.json ~/iio-data/minio/config

echo "Prepare docker-compose.yml..."
cat template.docker-compose.yml | sed "s/_user_/$USER/g" > docker-compose.yml
cat template.docker-compose-couch.yml | sed "s/_user_/$USER/g" > docker-compose-couch.yml

echo "Prepare dev start bash file..."
cp tools/helpers/_start.sh dev_start.sh
cp tools/helpers/_start-couch.sh dev_start-couch.sh
